# abcmn

Parse [abc music notation](http://abcnotation.com/) to Python data structures.

## Author

Steffen Brinkmann <s-b@mailbox.org>

## Licence

© 2018 Steffen Brinkmann, published under the terms of the 
[MIT License](http://www.opensource.org/licenses/MIT)