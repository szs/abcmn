'''Module for parsing abc music notation.'''

import re
from collections import deque
import sys
from typing import List, Dict, Tuple, Union, IO, Any, Optional

def parse(abc_input: Union[str, IO[str]]) -> Dict[str, Any]:
    '''Parse abc music notation.

    :param abc_input: a file or string containing abc music notation

    :returns: a dictionary containing the abc header and tunes
    '''

    if isinstance(abc_input, str):
        new_line_pattern = re.compile(r'\r\n?|\n')
        abc = [line.strip() for line in new_line_pattern.split(abc_input)]
    elif hasattr(abc_input, 'readlines'):
        abc = [line.strip() for line in abc_input.readlines()]
    else:
        raise TypeError(f"The argument for parse has type {type(abc_input)}, which cannot be parsed.")

    # at this point, abc is a list of strings containing all lines of the input

    result: Dict[str, Any] = dict()
    version, file_header, tunes = parse_coarse_structure(abc)

    result["version"] = version
    result["file_header"] = parse_file_header(file_header)
    result["tunes"] = parse_tunes(tunes)

    return result

def parse_coarse_structure(abc_input: List[str]) -> Tuple[Optional[str], Optional[List[str]], List[List[str]]]:
    '''parse the structure of the abc input on a very high level,
    only distinguiching verion string, header and tunes

    :param abc_input: a list of strings eith the complete abc input

    :returns: a tuple version, header, tunes. version is a string or None, header is a list of strings or None
    and tunes is a list of list of strings'''

    abc = deque(abc_input)
    if abc[0].startswith('%abc'):
        version = re.match(r'%abc-(\d+\.\d+)', abc[0]).groups()[0]
        abc.popleft()
    else:
        version = None

    # find next non empty line
    line = ''
    while line == '':
        line = abc.popleft()

    # optional file header
    if line.startswith('X:'):
        file_header = None
        tunes = [[line]]
    else:
        # find next non empty line, everything in between is the file header
        file_header = [line]
        header_line = abc.popleft()
        while header_line != '':
            file_header.append(header_line)
            header_line = abc.popleft()
        tunes = [[]]

    # tunes and free text
    while abc:
        line = abc.popleft()
        while line != '':
            tunes[-1].append(line)
            if not abc: break
            line = abc.popleft()
            #print("----------------\n\n", line, abc)
        else:
            tunes.append([])

    return version, file_header, tunes

def parse_file_header(file_header: List[str]) -> Optional[Dict[str, str]]:
    '''Parse the file header of an abc input.

    :param file_header: a list of strings containing the lines of the file header

    :returns: a dictionary with the contents of the file header or None
    '''

    if not file_header:
        return None

    result: Dict[str, Any] = dict()
    for line in file_header:
        if line.startswith('%%') or line.startswith('I:'):
            result.setdefault('stylesheet_directive', []).append(line[2:])
        elif line[1] == ':':
            result[line[0]] = line[2:]
        else:
            sys.stderr.write(f'File header line does not conform with the standard:\n{line}\n')

    return result

def parse_tunes(tunes: List[List[str]]) -> List[Dict[str, Any]]:
    '''Parse the tunes and free text of an abc input, i.e. everything after the file header.

    :param tunes: a list of tunes. tunes consist of a list of strings containing the lines of a tune

    :returns: a list of dictionaries with the contents of the tunes'''

    result: List[Dict[str, Any]] = []

    for tune in tunes:
        is_freetext = False if tune[0].startswith('X:') else True

        # free text
        if is_freetext:
            result.append({'type': 'freetext', 'data': tune})
            continue

        # tune
        result.append({'type': 'music', 'header': {}, 'body': []})
        is_header = True
        for line in tune:
            if is_header and line[1] != ':':
                sys.stderr.write(f'Tune header line does not conform with the standard:\n{line}\n')
                continue

            # tune header
            if is_header:
                result[-1]['header'][line[0]] = line[2:]
                if line.startswith('K:'):
                    is_header = False
            # tune body
            else:
                result[-1]['body'].append(line)

    return result

def parse_music_code(tune_header: Dict[str, str],
                  tune_body: List[str],
                  standard_pitch: float=440.) -> List[Tuple[float, float]]:
    '''convert a tune to a list of tuples of frequencies and lengths of notes.
    :param tune_header: the header of the tune.
    :param tune_header: the header of the tune.
    :param tune_header: the header of the tune.
    :returns: a list of tuples containing the frequency and legth of each note or chord
    '''

    freq_factor = standard_pitch / 440.

    unit_note = tune_header.get('L', '1/4')
    #meter =  tune_header.get('M', '4/4')
    tempo = tune_header.get('Q', '1/4=120').split('=')
    bpm = float(tempo[1])
    if tempo[0] == '1/8': bpm /= 2
    elif tempo[0] == '1/2': bpm *= 2

    result = []
    for line in tune_body:
        if line[1] == ':':
            if line[0] == 'L':
                unit_note = tune_header.get('L', '1/4')
            elif line[0] == 'Q':
                tempo = tune_header.get('Q', '1/4=120').split('/')
                bpm = float(tempo[1])
                if tempo[0] == '1/8': bpm /= 2
                elif tempo[0] == '1/2': bpm *= 2
            continue

        note_re = re.compile("(_{,2}\^{,2})([a-gA-G])([0-9/]*)([,'])*")
        result.extend(note_re.findall(line))

    return result
