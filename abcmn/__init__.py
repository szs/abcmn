'''Parse and convert abc music notation.
exports:
  function parse(abc): parse file or string containing abc music notation
'''

from .parse import (parse,
                    parse_coarse_structure,
                    parse_file_header,
                    parse_tunes,
                    parse_music_code)
